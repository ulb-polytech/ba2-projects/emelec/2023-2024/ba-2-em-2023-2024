/*
 * Programme de test des moteurs du projet BA2 EM-EIT.
 * Le schéma du circuit se trouve dans le dépôt (exemples/testMoteur).
 * L'Arduino Nano Every est connecté à un driver DRI0044
 * (https://wiki.dfrobot.com/2x1.2A_DC_Motor_Driver__TB6612FNG__SKU__DRI0044) : 
 * D2 <-> DIR1
 * D3 <-> PWM1
 * Les signaux de direction peuvent être connectés à d'autres "digital pins".
 * Par contre, les signaux PWM doivent être connectés aux pattes compatibles 
 * de l'Arduino (voir le pinout de l'Arduino).
 * 
 * On connecte un seul moteur, on n'a donc pas besoin du 2ème H-bridge.
 * On peut laisser les pattes DIR2 et PWM2 non connectées.
 * 
 * Le code fait varier la vitesse du moteur linéairement sur toute sa plage de fonctionnement.
 */

/* On utilise des #define pour pouvoir utiliser un nom qui a du sens pour les pattes.
 *  Cela rend le code plus lisible.
 *  C'est également utile si on doit changer de pattes au cours du projet : on ne doit modifier qu'une ligne de code .
 */

// Pattes de commande du moteur
#define DIR  2
#define PWM  3
// Durée des paliers de vitesse, définit l'accélaration du moteur, en ms
#define DELAY  5

void setup() {
  // configure la patte DIR en sortie digitale et l'initialise à l'état bas.
  pinMode(DIR, OUTPUT);
  digitalWrite(DIR, LOW);

  // on attend d'abord 2 sec avant de démarrer
  delay(2000);
}

void loop() {
  int pwmValue = 0; // variable pour stocker la valeur de la pwm actuelle

  // on commence par accélèrer de la vitesse nulle à la vitesse max dans le sens "LOW"
  digitalWrite(DIR, LOW);
  while (pwmValue < 256) {
    pwmValue++;
    analogWrite(PWM, pwmValue);
    delay(DELAY);
  }
  // on décélère jusqu'à la vitesse nulle
  while (pwmValue >= 0) {
    pwmValue--;
    analogWrite(PWM, pwmValue);
    delay(DELAY);
  }

  // on accélère de la vitesse nulle à la vitesse max dans le sens "HIGH"
  digitalWrite(DIR, HIGH);
  while (pwmValue < 256) {
    pwmValue++;
    analogWrite(PWM, pwmValue);
    delay(DELAY);
  }
  // on décélère jusqu'à la vitesse nulle
  while (pwmValue >= 0) {
    pwmValue--;
    analogWrite(PWM, pwmValue);
    delay(DELAY);
  }
}
